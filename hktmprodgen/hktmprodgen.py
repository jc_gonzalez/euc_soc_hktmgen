#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generation of HK/TM SOC Products

This application generates a set of HK/TM products, from the data
stored in the ARES cluster, as retrieved from the MOC Data Center.

The products generated follow the set of criteria defined in the
provided configuration file.
"""
#----------------------------------------------------------------------

import os
import sys
import argparse
import zipfile

from pprint import pprint
from datetime import datetime, timedelta


#----------------------------------------------------------------------

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

PYTHON2 = False
PY_NAME = "python3"

import argparse
import logging

from ares.ares_retrieve.ares_retrieve import Retriever

logger = logging.getLogger()


#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

# Default configuration
NominalUserCfgFolder = os.path.join(os.getenv('HOME', '/tmp'),
                                    '.euclid/hktmprodgen', 'hktmprodgen-pyares.ini')

def configureLogs():
    logger.setLevel(logging.DEBUG)

    # Create handlers
    c_handler = logging.StreamHandler()
    c_handler.setLevel(logging.INFO)

    # Create formatters and add it to handlers
    c_format = logging.Formatter('%(asctime)s %(levelname).1s %(module)s:%(lineno)d %(message)s')
    c_handler.setFormatter(c_format)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    for lname in os.getenv('LOGGING_MODULES','').split(':'):
        lgr = logging.getLogger(lname)
        if not lgr.handlers: lgr.addHandler(c_handler)

def getArgs():
    '''
    Parse arguments from command line

    :return: args structure
    '''
    parser = argparse.ArgumentParser(description='ARES Data RetrieverTest script to retrieve data from ARES system',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', dest='config_file', default=NominalUserCfgFolder,
                        help='Configuration file to use (default:{})'.format(NominalUserCfgFolder))
    parser.add_argument('-f', '--from_pid', dest='from_pid', type=int, default=1,
                        help='Initial parameter identifier')
    parser.add_argument('-t', '--to_pid', dest='to_pid', type=int, default=1000,
                        help='Final parameter identifier')
    parser.add_argument('-F', '--from_date', dest='from_date', type=int, nargs=5,
                        help='Initial date in the format Y DOY h m s')
    parser.add_argument('-T', '--to_date', dest='to_date', type=int, nargs=5,
                        help='Final date in the format Y DOY h m s')
    parser.add_argument('-n', '--num_pids_per_file', dest='num_pids_per_file', type=int,
                        default=100, help='Maximum number of PIDs per file')
    parser.add_argument('-e', '--sys_elem', dest='sys_elem', default='TM',
                        help='Set System Element (default:TM)')
    parser.add_argument('-o', '--output_dir', dest='output_dir', default='.',
                        help='Specify output directory (default:.)')
    parser.add_argument('-z', '--zip', dest='zip', default=False, action='store_true',
                        help='Create zip file')
    return parser.parse_args()

def greetings():
    """
    Says hello
    """
    logger.info('='*60)
    logger.info('hktmprodgen - Generation of HK/TM SOC Products')
    logger.info('='*60)

def createZip(output_dir, file_list):
    """
    Creates the final zip file with all the generated files in the output directory
    """
    baseName = os.path.basename(file_list[-1])
    zipName = os.path.join(output_dir, '.'.join([os.path.splitext(baseName)[0], 'zip']))
    with zipfile.ZipFile(zipName, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for file in file_list:
            zipf.write(file)
    return zipName


def main():
    """
    Main program
    """
    configureLogs()

    args = getArgs()

    greetings()
    logger.info(f'Config.file: {args.config_file}')
    if not os.path.isfile(args.config_file):
        logger.fatal('ERROR: Cannot find config. file. Exiting.')

    # Set dates (add ms)
    fromDate = list(args.from_date) + [0]
    toDate = list(args.to_date) + [0]
    rqstm = 'pid'

    # File name template
    filename_tpl = 'EUC_SOC_HKTM_%F-%T_%f-%t_%YMD1T%hms1-%YMD2T%hms2'
    if rqstm == 'name':
        filename_tpl = 'EUC_SOC_HKTM_%F_%N_%YMD1T%hms1-%YMD2T%hms2'

    retriever = Retriever(cfg_file=args.config_file, rqst_mode='pid',
                          from_pid=args.from_pid, to_pid=args.to_pid,
                          pids_block=args.num_pids_per_file,
                          from_date=tuple(fromDate), to_date=tuple(toDate),
                          output_dir=args.output_dir, file_tpl=filename_tpl,
                          sys_elem=args.sys_elem)
    retr_time_total, conv_time_total, full_time_total, param_names_invalid, gen_files = retriever.run()

    logger.info('Generated files:')
    for file in gen_files:
        logger.info(' - {}'.format(file))

    if args.zip:
        zipFile = createZip(args.output_dir, gen_files)
        logger.info('Created package product: {}'.format(zipFile))


if __name__ == '__main__':
    main()
