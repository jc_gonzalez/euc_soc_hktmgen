#!/bin/bash 
#------------------------------------------------------------------------------------
# HKTMProductGenerator.sh
# HK/TM Product Generator
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2020 by Euclid SOC Team
#------------------------------------------------------------------------------------
SCRIPTPATH=$(cd $(dirname $0); pwd; cd - > /dev/null)
export PYARES_INI_FILE=$HOME/.euclid/hktmprodgen/hktmprodgen-pyares.ini
export PYTHONPATH=${SCRIPTPATH}

if [ ! -f ${PYARES_INI_FILE} ]; then
    echo "Expected PyARES config. file at ${PYARES_INI_FILE} not found"
    exit 1
fi

TMPDIR=/tmp/$$.dir
mkdir -p ${TMPDIR}

NEW_OPTS="-c ${PYARES_INI_FILE}"
arg="$1"
while [ -n "$arg" ]; do
    #echo "$arg"
    shift
    case "$arg" in
        "-o"|"--output_dir")
            OUTPUT_DIR=$1
            NEW_OPTS="$NEW_OPTS -o $TMPDIR"
            shift ;;
        *)
            NEW_OPTS="$NEW_OPTS $arg"
            ;;
    esac
    arg="$1"
done

${PYTHON:=python3} ${SCRIPTPATH}/hktmprodgen/hktmprodgen.py $NEW_OPTS -z

mv ${TMPDIR}/*.zip $OUTPUT_DIR/

# Clean up
#rm -rf ${TMPDIR}
