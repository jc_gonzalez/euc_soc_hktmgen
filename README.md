Euclid SOC HKTM Product Generator
==========================================

This directory and all its sub-directories contain the launcher and modules of the
Euclid SOC HK/TM Product Generator.

This tool takes a configuration file, specifying what HK/TM parameters should be
taken and how, to build a HK/TM Product, and retrieves the corresponding values 
for the range of dates specified, and saves them as defined in the config. file in a
series of FITS files.  In addition, the appropriate XML Metadata Object file is 
generated as well.

Installation and Execution
--------------------------

For installing and running this tool, just clone the `git` repository, and launch the script 
`HKTMProductGenerator.sh` to get a message on the valid command line arguments.

License
--------

You should have received a copy of the GNU Lesser General Public License
along with QPF (please, see [COPYING][1] and [COPYING.LESSER][2] for
information on this license).  If not, see <http://www.gnu.org/licenses/>.

[1]: ./COPYING
[2]: ./COPYING.LESSER
