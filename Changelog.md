Change Log
====================

All notable changes to the Euclid SOC HKTM Product Generator software project will be documented 
in this file.

V1.0 / 2020-05-12
--------------------------

First release of the Euclid SOC HKTM Product Generator.

